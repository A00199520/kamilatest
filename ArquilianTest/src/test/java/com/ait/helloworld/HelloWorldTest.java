package com.ait.helloworld;
import static org.junit.Assert.*;

import org.junit.Test;

import com.ait.helloworld.HelloWorld;

public class HelloWorldTest {

	@Test
	public void test() {
		HelloWorld myTest = new HelloWorld();
		assertEquals("Test Passed!", myTest.test());
	}

}
