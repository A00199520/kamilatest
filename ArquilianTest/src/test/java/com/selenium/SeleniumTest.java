package com.selenium;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumTest {
	private WebDriver driver;

//	@Test
//	public void testEasy() throws Exception {
//		driver.get("http://automationpractice.com");
//		driver.findElement(By.className("login")).click();
//		Thread.sleep(1000);
//		driver.findElement(By.id("email")).sendKeys("testMASE@gmail.com");
//		driver.findElement(By.id("passwd")).sendKeys("mase123");
//		driver.findElement(By.id("SubmitLogin")).click();
//		Thread.sleep(1000);
//		String verification = driver.findElement(By.xpath("html/body/div/div[2]/div/div[3]/div/p")).getText();
//		Thread.sleep(1000);
//		Assert.assertTrue(verification.contains(
//				"Welcome to your account. Here you can manage all of " + "your personal information and orders"));
//	}

	@Before
	public void beforeTest() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized", "--headless", "--no-sandbox");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver(options);
	}

	@After
	public void afterTest() {
		if (driver != null) {
            driver.quit();
        }
	}
}
